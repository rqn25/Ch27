
public class Hash {

public static void main(String[] args) {
	MySet<String> set = new MyHashSet<>();
	set.add("Smith");
	set.add("Anderson");
	set.add("Lewis");
	set.add("Cook");

	System.out.println("Names in set:");
	for (String s: set)
	System.out.println(s);
	System.out.println("\nNumber of names in set:");
	System.out.println(set.size());

	}

}
